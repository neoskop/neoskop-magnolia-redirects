# README

Allgemeine Beschreibung vom Modul **Redirects**.

# Abhängigkeiten

Bis Version 1.2.1:

- [Magnolia CMS][1] >= 5.4.5

  Ab Version 1.3.0:

- [Magnolia CMS][1] >= 5.4.5

  Ab Version 1.4.0:

- [Magnolia CMS][1] >= 6.2.11

# Installation

Das Modul muss in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-redirects</artifactId>
	<version>1.3.0</version>
</dependency>
```

Mehr Informationen zur Benutzung

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
