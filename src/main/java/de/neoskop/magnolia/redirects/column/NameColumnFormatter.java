package de.neoskop.magnolia.redirects.column;

import static de.neoskop.magnolia.redirects.RedirectsModule.ALIASES_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.DOMAIN;
import static de.neoskop.magnolia.redirects.RedirectsModule.NAME_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.RULE;

import com.google.common.collect.Iterables;
import com.vaadin.v7.ui.Table;
import de.neoskop.magnolia.redirects.domain.Rule;
import de.neoskop.magnolia.redirects.domain.RuleFactory;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.PropertyColumnDefinition;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
public class NameColumnFormatter extends AbstractColumnFormatter<PropertyColumnDefinition> {

  private static final Logger LOG = LogManager.getLogger(NameColumnFormatter.class);

  @Inject
  public NameColumnFormatter(PropertyColumnDefinition definition) {
    super(definition);
  }

  @Override
  public Object generateCell(Table source, Object itemId, Object columnId) {
    final Item jcrItem = getJcrItem(source, itemId);

    if (jcrItem == null || !jcrItem.isNode()) {
      return "";
    }

    Node node = (Node) jcrItem;

    try {
      if (node.isNodeType(DOMAIN)) {
        final StringBuilder sb = new StringBuilder(node.getProperty(NAME_PROPERTY).getString());

        if (node.hasProperty(ALIASES_PROPERTY)) {
          Stream.of(node.getProperty(ALIASES_PROPERTY).getValues())
              .forEach(
                  v -> {
                    try {
                      sb.append(", ").append(v.getString());
                    } catch (RepositoryException e) {
                      LOG.error(
                          "Could not get value of "
                              + v
                              + " for node "
                              + NodeUtil.getPathIfPossible(node),
                          e);
                    }
                  });
        }

        sb.append(" (").append(Iterables.size(NodeUtil.getNodes(node, RULE))).append(")");
        return StringEscapeUtils.escapeHtml4(sb.toString());
      } else if (node.isNodeType(RULE)) {
        final Rule rule = RuleFactory.getRuleFromNode(node);

        if (rule != null) {
          return StringEscapeUtils.escapeHtml4(rule.toString());
        }
      }
    } catch (RepositoryException e) {
      LOG.error("Could not get name of " + NodeUtil.getPathIfPossible(node));
    }

    return "";
  }
}
