package de.neoskop.magnolia.redirects.column;

import static de.neoskop.magnolia.redirects.RedirectsModule.RULE;
import static de.neoskop.magnolia.redirects.RedirectsModule.TYPE_PROPERTY;

import com.vaadin.v7.ui.Table;
import de.neoskop.magnolia.redirects.domain.RedirectType;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.PropertyColumnDefinition;
import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 15.12.16
 */
public class RedirectTypeColumnFormatter extends AbstractColumnFormatter<PropertyColumnDefinition> {

  private static final Logger LOG = LogManager.getLogger(RedirectTypeColumnFormatter.class);

  @Inject
  public RedirectTypeColumnFormatter(PropertyColumnDefinition definition) {
    super(definition);
  }

  @Override
  public Object generateCell(Table source, Object itemId, Object columnId) {
    final Item jcrItem = getJcrItem(source, itemId);

    if (jcrItem == null || !jcrItem.isNode()) {
      return "";
    }

    Node node = (Node) jcrItem;

    try {
      if (node.isNodeType(RULE) && node.hasProperty(TYPE_PROPERTY)) {
        return RedirectType.findByJcrValue(node.getProperty(TYPE_PROPERTY).getString());
      }
    } catch (RepositoryException e) {
      LOG.error("Could not get name of " + NodeUtil.getPathIfPossible(node));
    }

    return "";
  }
}
