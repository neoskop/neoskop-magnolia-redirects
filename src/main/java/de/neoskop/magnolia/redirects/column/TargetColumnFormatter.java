package de.neoskop.magnolia.redirects.column;

import com.vaadin.v7.ui.Table;
import de.neoskop.magnolia.redirects.domain.Rule;
import de.neoskop.magnolia.redirects.domain.RuleFactory;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.PropertyColumnDefinition;
import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 23.11.16
 */
public class TargetColumnFormatter extends AbstractColumnFormatter<PropertyColumnDefinition> {

  private static final Logger LOG = LogManager.getLogger(TargetColumnFormatter.class);

  @Inject
  public TargetColumnFormatter(PropertyColumnDefinition definition) {
    super(definition);
  }

  @Override
  public Object generateCell(Table source, Object itemId, Object columnId) {
    final Item jcrItem = getJcrItem(source, itemId);

    if (jcrItem == null || !jcrItem.isNode()) {
      return "";
    }

    final Node node = (Node) jcrItem;

    try {
      if (node.getDepth() == 0) {
        return "/";
      }
    } catch (RepositoryException e) {
      LOG.error("Could not determine depth of node " + NodeUtil.getPathIfPossible(node), e);
    }

    final Rule rule = RuleFactory.getRuleFromNode(node);

    if (rule != null) {
      return StringEscapeUtils.escapeHtml4(rule.getDisplayTarget());
    }

    return "";
  }
}
