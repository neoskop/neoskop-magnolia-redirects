package de.neoskop.magnolia.redirects.definition;

import de.neoskop.magnolia.redirects.action.ExportRulesAsCSVAction;
import info.magnolia.ui.api.action.CommandActionDefinition;

/**
 * @author Arne Diekmann
 * @since 03.03.17
 */
public class ExportRulesAsCSVActionDefinition extends CommandActionDefinition {

  public ExportRulesAsCSVActionDefinition() {
    setImplementationClass(ExportRulesAsCSVAction.class);
  }
}
