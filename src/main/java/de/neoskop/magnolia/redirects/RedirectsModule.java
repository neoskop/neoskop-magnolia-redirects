package de.neoskop.magnolia.redirects;

import de.neoskop.magnolia.redirects.service.RedirectsService;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;

public class RedirectsModule implements ModuleLifecycle {

  public static final String WORKSPACE = "redirects";
  public static final String DOMAIN = "mgnl:domain";
  public static final String RULE = "mgnl:redirectRule";
  public static final String TYPE_PROPERTY = "type";
  public static final String RULE_PROPERTY = "rule";
  public static final String NAME_PROPERTY = "name";
  public static final String HEADER_NAME_PROPERTY = "headerName";
  public static final String HEADER_VALUE_PROPERTY = "headerValue";
  public static final String REGEX_PROPERTY = "regex";
  public static final String TARGET_PROPERTY = "target";
  public static final String URL_PROPERTY = "url";
  public static final String TARGETURL_PROPERTY = "targeturl";
  public static final String PAGE_PROPERTY = "page";
  public static final String DOMAIN_PROPERTY = "domain";
  public static final String ALIASES_PROPERTY = "aliases";
  private boolean forceSecure = true;

  public boolean isForceSecure() {
    return forceSecure;
  }

  public void setForceSecure(boolean forceSecure) {
    this.forceSecure = forceSecure;
  }

  @Override
  public void start(ModuleLifecycleContext moduleLifecycleContext) {
    MgnlContext.doInSystemContext(
        () -> {
          Components.getComponent(RedirectsService.class).warmUpRuleCache();
          return true;
        });
  }

  @Override
  public void stop(ModuleLifecycleContext moduleLifecycleContext) {}
}
