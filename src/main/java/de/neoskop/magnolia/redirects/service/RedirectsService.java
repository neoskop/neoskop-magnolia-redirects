package de.neoskop.magnolia.redirects.service;

import static de.neoskop.magnolia.redirects.RedirectsModule.ALIASES_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.DOMAIN;
import static de.neoskop.magnolia.redirects.RedirectsModule.NAME_PROPERTY;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.google.common.base.Charsets;
import de.neoskop.magnolia.redirects.RedirectsModule;
import de.neoskop.magnolia.redirects.domain.Rule;
import de.neoskop.magnolia.redirects.domain.RuleFactory;
import info.magnolia.cms.security.JCRSessionOp;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 22.11.16
 */
@Singleton
public class RedirectsService {

  private static final Logger LOG = LogManager.getLogger(RedirectsService.class);
  private static final Pattern REDIRECT_OR_REWRITE_PATTERN =
      Pattern.compile("^\\s*(RewriteRule|Redirect)");
  private final LoadingCache<String, List<Rule>> ruleCache =
      Caffeine.newBuilder().removalListener(this::cacheKeyRemoved).build(this::getRulesForDomain);

  private void cacheKeyRemoved(String key, List<Rule> value, RemovalCause cause) {
    LOG.debug("Cache for domain " + key + " was evicted: " + cause);
  }

  private List<Rule> getRulesForDomain(String domain) {
    final List<Rule> result = new ArrayList<>();
    long startTime = System.nanoTime();

    try {
      final Session session = MgnlContext.getJCRSession(RedirectsModule.WORKSPACE);
      NodeUtil.getNodes(session.getRootNode(), DOMAIN)
          .forEach(d -> addMatchingDomainRules(domain, result, d));
    } catch (RepositoryException e) {
      LOG.error("Querying of redirect rules for " + domain + " failed", e);
    }

    long estimatedTime = System.nanoTime() - startTime;
    final long milliseconds = TimeUnit.MILLISECONDS.convert(estimatedTime, TimeUnit.NANOSECONDS);
    LOG.debug("Loading of rules for " + domain + " took: " + milliseconds + "ms");
    return result.stream().filter(Objects::nonNull).collect(Collectors.toList());
  }

  private void addMatchingDomainRules(String domain, List<Rule> result, Node d) {
    try {
      boolean matches = false;

      if (d.hasProperty(NAME_PROPERTY)) {
        matches = d.getProperty(NAME_PROPERTY).getString().equalsIgnoreCase(domain);
      }

      if (!matches && d.hasProperty(ALIASES_PROPERTY)) {
        matches =
            PropertyUtil.getValuesStringList(d.getProperty(ALIASES_PROPERTY).getValues())
                .stream()
                .anyMatch(a -> a.equalsIgnoreCase(domain));
      }

      if (matches) {
        NodeUtil.getNodes(d, RedirectsModule.RULE)
            .forEach(c -> result.add(RuleFactory.getRuleFromNode(c)));
      }
    } catch (RepositoryException e) {
      LOG.error("Could not ", e);
    }
  }

  public boolean process(HttpServletRequest request, HttpServletResponse response) {
    final List<Rule> rules = ruleCache.get(request.getServerName());

    if (rules == null) {
      return false;
    }

    final Optional<Rule> ruleOptional =
        rules.stream().filter(r -> r.requestMatches(request)).findAny();

    if (ruleOptional.isPresent()) {
      redirectToRuleTarget(request, response, ruleOptional.get());
      return true;
    }

    return false;
  }

  public void warmUpRuleCache() {
    try {
      findAllRules();
    } catch (RepositoryException e) {
      LOG.error("Warm-up of rule cache failed", e);
    }
  }

  private Map<String, List<Rule>> loadRulesOfDomainNode(Node n) {
    List<String> domainNames = new ArrayList<>();

    try {
      if (n.hasProperty(NAME_PROPERTY)) {
        domainNames.add(n.getProperty(NAME_PROPERTY).getString());
      }

      if (n.hasProperty(ALIASES_PROPERTY) && n.getProperty(ALIASES_PROPERTY).isMultiple()) {
        domainNames.addAll(
            PropertyUtil.getValuesStringList(n.getProperty(ALIASES_PROPERTY).getValues()));
      }
    } catch (RepositoryException e) {
      LOG.warn("Could not load aliases", e);
    }

    return ruleCache.getAll(domainNames);
  }

  public void evictRuleCache() {
    ruleCache.invalidateAll();
    try {
      MgnlContext.doInSystemContext(
          new JCRSessionOp<Void>(RedirectsModule.WORKSPACE) {
            @Override
            public Void exec(Session session) throws RepositoryException {
              warmUpRuleCache();
              return null;
            }
          });
    } catch (RepositoryException e) {
      LOG.warn("Warm-up after eviction failed", e);
    }
  }

  public Map<String, List<Rule>> findAllRules() throws RepositoryException {
    Map<String, List<Rule>> result = new HashMap<>();
    final Session jcrSession = MgnlContext.getJCRSession(RedirectsModule.WORKSPACE);
    NodeUtil.getNodes(jcrSession.getRootNode(), DOMAIN)
        .forEach(
            n -> {
              final Map<String, List<Rule>> rules = loadRulesOfDomainNode(n);
              result.putAll(rules);
            });
    return result;
  }

  private void redirectToRuleTarget(
      HttpServletRequest request, HttpServletResponse response, Rule rule) {
    LOG.debug(
        "Will redirect ("
            + rule.getRedirectType()
            + ") '"
            + request.getRequestURL()
            + "' to '"
            + rule.getTarget(request)
            + "' as "
            + rule
            + " matches");

    switch (rule.getRedirectType()) {
      case PERMANENT:
        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        break;

      case TEMPORARY:
        response.setStatus(HttpServletResponse.SC_FOUND);
        break;
    }

    response.setHeader("Location", rule.getTarget(request));
  }

  public void importApacheConfig(InputStream stream, final String path) throws IOException {
    try (BufferedReader buffer =
        new BufferedReader(new InputStreamReader(stream, Charsets.UTF_8))) {
      buffer
          .lines()
          .filter(this::isRedirectOrRewriteRule)
          .map(c -> RuleFactory.getRuleFromApacheConfig(c, path))
          .filter(Objects::nonNull)
          .forEach(this::persistRule);
    }
  }

  private void persistRule(Rule rule) {
    try {
      rule.getNode().getSession().save();
    } catch (RepositoryException e) {
      LOG.error("Could not persist node for rule " + rule, e);
    }
  }

  private boolean isRedirectOrRewriteRule(String configLine) {
    return REDIRECT_OR_REWRITE_PATTERN.matcher(configLine).find();
  }
}
