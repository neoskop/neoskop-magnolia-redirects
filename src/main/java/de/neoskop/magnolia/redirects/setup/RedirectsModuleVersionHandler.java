package de.neoskop.magnolia.redirects.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleModuleResource;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.FilterOrderingTask;
import info.magnolia.module.delta.IsAdminInstanceDelegateTask;
import info.magnolia.module.delta.SetPropertyTask;
import info.magnolia.module.delta.Task;
import info.magnolia.repository.RepositoryConstants;
import java.util.Arrays;
import java.util.List;

public class RedirectsModuleVersionHandler extends DefaultModuleVersionHandler {

  private static final String REDIRECTS_FILTERS_PATH = "/server/filters/redirects";

  public RedirectsModuleVersionHandler() {
    register(
        DeltaBuilder.update("1.0.3", "")
            .addTask(
                new BootstrapSingleModuleResource(
                    "config.modules.observation.config.listenerConfigurations.evictRuleCache.xml")));
    register(
        DeltaBuilder.update("1.0.4", "")
            .addTask(new BootstrapSingleModuleResource("config.modules.redirects.commands.xml")));
    register(
        DeltaBuilder.update("1.0.8", "")
            .addTask(new BootstrapSingleModuleResource("config.modules.redirects.commands.xml")));
    register(DeltaBuilder.update("1.0.9", "").addTask(new MigrateTargetUrlTask()));
  }

  @Override
  protected List<Task> getExtraInstallTasks(InstallContext installContext) {
    return Arrays.asList(
        new FilterOrderingTask("redirects", new String[] {"context"}),
        new IsAdminInstanceDelegateTask(
            "",
            new SetPropertyTask(
                RepositoryConstants.CONFIG, REDIRECTS_FILTERS_PATH, "enabled", "false")));
  }
}
