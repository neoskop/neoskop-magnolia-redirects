package de.neoskop.magnolia.redirects.setup;

import static de.neoskop.magnolia.redirects.RedirectsModule.RULE;
import static javax.jcr.query.Query.JCR_SQL2;

import de.neoskop.magnolia.redirects.RedirectsModule;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.AbstractRepositoryTask;
import info.magnolia.module.delta.TaskExecutionException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Arne Diekmann
 * @since 15.08.17
 */
public class MigrateTargetUrlTask extends AbstractRepositoryTask {

  MigrateTargetUrlTask() {
    super(null, null);
  }

  @Override
  protected void doExecute(InstallContext ctx) throws RepositoryException, TaskExecutionException {
    final Session session = ctx.getJCRSession(RedirectsModule.WORKSPACE);
    final Query query =
        session
            .getWorkspace()
            .getQueryManager()
            .createQuery("SELECT * FROM [" + RULE + "]", JCR_SQL2);
    final NodeIterator nodeIt = query.execute().getNodes();

    while (nodeIt.hasNext()) {
      final Node node = nodeIt.nextNode();
      final String target = (String) new ContentMap(node).get(RedirectsModule.TARGET_PROPERTY);

      if (StringUtils.equalsIgnoreCase(target, "url")) {
        node.setProperty(RedirectsModule.TARGET_PROPERTY, RedirectsModule.TARGETURL_PROPERTY);
        PropertyUtil.renameProperty(node.getProperty("url"), RedirectsModule.TARGETURL_PROPERTY);
      }
    }

    session.save();
  }
}
