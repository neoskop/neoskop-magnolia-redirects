package de.neoskop.magnolia.redirects.action;

import com.vaadin.server.Page;
import de.neoskop.magnolia.redirects.definition.ExportRulesAsCSVActionDefinition;
import info.magnolia.commands.CommandsManager;
import info.magnolia.context.MgnlContext;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.framework.action.AbstractCommandAction;
import info.magnolia.ui.framework.util.TempFileStreamResource;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;
import javax.jcr.Item;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Arne Diekmann
 * @since 03.03.17
 */
public class ExportRulesAsCSVAction
    extends AbstractCommandAction<ExportRulesAsCSVActionDefinition> {

  private TempFileStreamResource tempFileStreamResource;

  @Inject
  public ExportRulesAsCSVAction(
      ExportRulesAsCSVActionDefinition definition,
      JcrItemAdapter item,
      CommandsManager commandsManager,
      UiContext uiContext,
      SimpleTranslator i18n)
      throws ActionExecutionException {
    super(definition, item, commandsManager, uiContext, i18n);
  }

  @Override
  protected void onPreExecute() throws Exception {
    tempFileStreamResource = new TempFileStreamResource();
    tempFileStreamResource.setTempFileName("redirects");
    tempFileStreamResource.setTempFileExtension("csv");
    super.onPreExecute();
  }

  @Override
  protected void onPostExecute() throws Exception {
    HttpServletRequest request = MgnlContext.getWebContext().getRequest();
    final String forwardedHost = request.getHeader("Host");
    this.tempFileStreamResource.setFilename(
        StringUtils.isBlank(forwardedHost)
            ? "redirects.csv"
            : "redirects_" + forwardedHost + ".csv");
    this.tempFileStreamResource.setMIMEType("text/csv");
    Page.getCurrent().open(this.tempFileStreamResource, "", true);
  }

  @Override
  protected Map<String, Object> buildParams(Item jcrItem) {
    Map<String, Object> params = super.buildParams(jcrItem);

    try {
      params.put("outputStream", this.tempFileStreamResource.getTempFileOutputStream());
      return params;
    } catch (IOException e) {
      throw new IllegalStateException("Failed to bind command to temp file output stream", e);
    }
  }
}
