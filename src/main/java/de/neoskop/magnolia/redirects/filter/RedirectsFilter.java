package de.neoskop.magnolia.redirects.filter;

import de.neoskop.magnolia.redirects.service.RedirectsService;
import info.magnolia.cms.filters.AbstractMgnlFilter;
import info.magnolia.objectfactory.Components;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
public class RedirectsFilter extends AbstractMgnlFilter {

  @Override
  public void doFilter(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    final RedirectsService redirectsService = Components.getComponent(RedirectsService.class);

    if (redirectsService.process(request, response)) {
      return;
    }

    filterChain.doFilter(request, response);
  }
}
