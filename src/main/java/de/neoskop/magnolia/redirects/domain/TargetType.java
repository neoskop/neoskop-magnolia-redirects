package de.neoskop.magnolia.redirects.domain;

/**
 * @author Arne Diekmann
 * @since 15.12.16
 */
public enum TargetType {
  PAGE,
  TARGETURL;

  public static TargetType findByJcrValue(String jcrValue) {
    return valueOf(jcrValue.toUpperCase());
  }

  public String toJcrValue() {
    return name().toLowerCase();
  }
}
