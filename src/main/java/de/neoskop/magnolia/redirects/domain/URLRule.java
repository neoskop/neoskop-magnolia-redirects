package de.neoskop.magnolia.redirects.domain;

import static de.neoskop.magnolia.redirects.RedirectsModule.URL_PROPERTY;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
class URLRule extends Rule {

  private final String url;

  URLRule(Node node) {
    super(node);
    this.url = getProperty(node, URL_PROPERTY);
  }

  @Override
  public boolean requestMatches(HttpServletRequest request) {
    return request.getRequestURI().startsWith(url);
  }

  @Override
  public String toString() {
    return url;
  }
}
