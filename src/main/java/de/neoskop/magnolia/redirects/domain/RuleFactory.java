package de.neoskop.magnolia.redirects.domain;

import static de.neoskop.magnolia.redirects.RedirectsModule.DOMAIN_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.PAGE_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.REGEX_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.RULE;
import static de.neoskop.magnolia.redirects.RedirectsModule.RULE_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.TARGETURL_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.TARGET_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.TYPE_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.WORKSPACE;
import static de.neoskop.magnolia.redirects.domain.RedirectType.PERMANENT;
import static de.neoskop.magnolia.redirects.domain.RedirectType.TEMPORARY;
import static de.neoskop.magnolia.redirects.domain.RuleType.REGEX;
import static info.magnolia.repository.RepositoryConstants.WEBSITE;

import de.neoskop.magnolia.redirects.RedirectsModule;
import info.magnolia.cms.core.Path;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.SessionUtil;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
public class RuleFactory {

  private static final Logger LOG = LogManager.getLogger(RuleFactory.class);
  private static final Pattern REWRITE_RULE_PATTERN =
      Pattern.compile("RewriteRule\\s+\"?(.+?)\"?\\s+\"?(.+)\"?\\s+\\[(.+)]");

  public static Rule getRuleFromNode(Node node) {
    try {
      if (!node.hasProperty(RULE_PROPERTY)) {
        return null;
      }

      final String jcrValue = node.getProperty(RULE_PROPERTY).getString();
      final RuleType ruleType = RuleType.findByJcrValue(jcrValue);

      if (ruleType == null) {
        return null;
      }

      if (!TargetType.TARGETURL
          .toJcrValue()
          .equals(node.getProperty(RedirectsModule.TARGET_PROPERTY).getString())) {
        Session jcrSession = MgnlContext.getJCRSession(WEBSITE);
        final String targetPageUUID = node.getProperty(PAGE_PROPERTY).getString();
        jcrSession.getNodeByIdentifier(targetPageUUID);
      }

      switch (ruleType) {
        case HTTP_HEADER:
          return new HTTPHeaderRule(node);
        case URL:
          return new URLRule(node);
        case REGEX:
          return new RegexRule(node);
      }
    } catch (ItemNotFoundException e) {
      LOG.debug("Referenced page of rule " + NodeUtil.getPathIfPossible(node) + " does not exist");
    } catch (RepositoryException e) {
      LOG.error("Could not access rule property", e);
    }

    return null;
  }

  public static Rule getRuleFromApacheConfig(String configLine, String path) {
    if (configLine.contains("RewriteRule")) {
      return getRuleFromRewriteStatement(configLine, path);
    } else if (configLine.contains("Redirect")) {
      // TODO: Implement Redirect statements
    }

    return null;
  }

  private static Rule getRuleFromRewriteStatement(String configLine, String path) {
    final Matcher matcher = REWRITE_RULE_PATTERN.matcher(configLine);

    if (!matcher.find()) {
      LOG.warn(
          "Line '"
              + configLine
              + "' does not match pattern '"
              + REWRITE_RULE_PATTERN.pattern()
              + "'");
      return null;
    }

    final String regex = matcher.group(1);
    final String target = matcher.group(2);
    final List<String> options = Arrays.asList(matcher.group(3).split(","));
    LOG.warn(
        "Matched regex '"
            + regex
            + "', target '"
            + target
            + "', options "
            + options.stream().collect(Collectors.joining(", ")));

    if (!options.contains("L") || options.stream().noneMatch(RuleFactory::isARedirectFlag)) {
      LOG.warn("Rule '" + configLine + "' is not last or is not a redirect");
      return null;
    }

    try {
      final Session session = MgnlContext.getJCRSession(WORKSPACE);
      final String newNodeName = Path.getUniqueLabel(session, path, "0");
      final Node ruleNode = session.getNode(path).addNode(newNodeName, RULE);
      ruleNode.setProperty(RULE_PROPERTY, REGEX.getJcrValue());
      ruleNode.setProperty(REGEX_PROPERTY, regex);
      ruleNode.setProperty(
          TYPE_PROPERTY,
          options.contains("R=301") ? PERMANENT.toJcrValue() : TEMPORARY.toJcrValue());
      setTarget(ruleNode, target, path.substring(1));
      return getRuleFromNode(ruleNode);
    } catch (RepositoryException e) {
      LOG.error("Could not create rule node", e);
    } catch (MalformedURLException e) {
      LOG.warn("Target URL '" + target + "' was malformed", e);
    }

    return null;
  }

  private static boolean isARedirectFlag(String flag) {
    return flag.startsWith("R");
  }

  private static void setTarget(Node ruleNode, String target, String domain)
      throws RepositoryException, MalformedURLException {
    URL url = new URL("https", domain, target);

    if (target.startsWith("http://") || target.startsWith("https://")) {
      url = new URL(target);
    }

    final Node pageNode = SessionUtil.getNode(WEBSITE, url.getPath());

    if (pageNode != null) {
      ruleNode.setProperty(TARGET_PROPERTY, TargetType.PAGE.toJcrValue());
      ruleNode.setProperty(PAGE_PROPERTY, pageNode.getIdentifier());

      if (!url.getHost().equalsIgnoreCase(domain)) {
        ruleNode.setProperty(DOMAIN_PROPERTY, url.getHost());
      }
    } else {
      ruleNode.setProperty(TARGET_PROPERTY, TargetType.TARGETURL.toJcrValue());
      ruleNode.setProperty(TARGETURL_PROPERTY, target);
    }
  }
}
