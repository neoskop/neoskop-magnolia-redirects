package de.neoskop.magnolia.redirects.domain;

import static de.neoskop.magnolia.redirects.RedirectsModule.REGEX_PROPERTY;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
class RegexRule extends Rule {

  private final String regex;
  private final Pattern pattern;
  private String target;

  RegexRule(Node node) {
    super(node);
    regex = getProperty(node, REGEX_PROPERTY);
    pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
  }

  @Override
  public boolean requestMatches(HttpServletRequest request) {
    Matcher matcher = pattern.matcher(request.getRequestURI());

    if (matcher.matches()) {
      target = matcher.replaceAll(super.getTarget(request));
      return true;
    }

    return false;
  }

  @Override
  public String getTarget(HttpServletRequest request) {
    if (request.getQueryString() != null && !target.contains("?")) {
      return target + "?" + request.getQueryString();
    }

    return target;
  }

  @Override
  public String toString() {
    return regex;
  }
}
