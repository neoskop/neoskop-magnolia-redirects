package de.neoskop.magnolia.redirects.domain;

import static de.neoskop.magnolia.redirects.RedirectsModule.HEADER_NAME_PROPERTY;
import static de.neoskop.magnolia.redirects.RedirectsModule.HEADER_VALUE_PROPERTY;

import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
class HTTPHeaderRule extends Rule {

  private final String headerName;
  private final String headerValue;
  private final String uri;
  private final Pattern pattern;

  HTTPHeaderRule(Node node) {
    super(node);
    this.headerName = getProperty(node, HEADER_NAME_PROPERTY);
    this.headerValue = getProperty(node, HEADER_VALUE_PROPERTY);
    this.uri = getProperty(node, "uri");
    this.pattern = Pattern.compile(headerValue, Pattern.CASE_INSENSITIVE);
  }

  @Override
  public boolean requestMatches(HttpServletRequest request) {
    if (StringUtils.isNotBlank(uri) && !request.getRequestURI().equalsIgnoreCase(uri)) {
      return false;
    }

    final Enumeration headerNames = request.getHeaderNames();

    while (headerNames.hasMoreElements()) {
      final String headerName = (String) headerNames.nextElement();

      if (headerName.equalsIgnoreCase(this.headerName)) {
        final String headerValue = request.getHeader(headerName);
        final Matcher matcher = pattern.matcher(headerValue);
        return matcher.find();
      }
    }

    return false;
  }

  @Override
  public String toString() {
    return uri + " ∧ " + headerName + "=" + headerValue;
  }
}
