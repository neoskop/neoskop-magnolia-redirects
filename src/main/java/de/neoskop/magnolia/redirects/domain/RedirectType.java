package de.neoskop.magnolia.redirects.domain;

/**
 * @author Arne Diekmann
 * @since 21.11.16
 */
public enum RedirectType {
  PERMANENT("301"),
  TEMPORARY("302");

  private final String title;

  RedirectType(String title) {
    this.title = title;
  }

  public static RedirectType findByJcrValue(String jcrValue) {
    return valueOf(jcrValue.toUpperCase());
  }

  public String toJcrValue() {
    return name().toLowerCase();
  }

  @Override
  public String toString() {
    return title;
  }
}
