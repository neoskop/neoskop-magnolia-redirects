package de.neoskop.magnolia.redirects.domain;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
public enum RuleType {
  HTTP_HEADER("httpHeader"),
  URL("url"),
  REGEX("regex");

  private final String jcrValue;

  RuleType(String jcrValue) {
    this.jcrValue = jcrValue;
  }

  public static RuleType findByJcrValue(String jcrValue) {
    for (RuleType ruleType : values()) {
      if (ruleType.getJcrValue().equals(jcrValue)) {
        return ruleType;
      }
    }

    return null;
  }

  public String getJcrValue() {
    return jcrValue;
  }
}
