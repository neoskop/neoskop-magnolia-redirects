package de.neoskop.magnolia.redirects.domain;

import static info.magnolia.repository.RepositoryConstants.WEBSITE;

import de.neoskop.magnolia.redirects.RedirectsModule;
import info.magnolia.context.MgnlContext;
import info.magnolia.objectfactory.Components;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 18.11.16
 */
public abstract class Rule {

  private static final Logger LOG = LogManager.getLogger(Rule.class);
  private final boolean isAbsoluteUrl;
  private final RedirectType redirectType;
  private final Node node;
  private String targetUrl;
  private String targetUri;
  private String targetDomain;

  Rule(Node node) {
    final String targetType = getProperty(node, RedirectsModule.TARGET_PROPERTY);
    this.node = node;
    this.isAbsoluteUrl = targetType.equals(TargetType.TARGETURL.toJcrValue());

    if (isAbsoluteUrl) {
      this.targetUrl = getProperty(node, RedirectsModule.TARGETURL_PROPERTY);
    } else {
      final String targetPageUUID = getProperty(node, RedirectsModule.PAGE_PROPERTY);
      String pageNodePath = null;

      try {
        Session jcrSession = MgnlContext.getJCRSession(WEBSITE);
        pageNodePath = jcrSession.getNodeByIdentifier(targetPageUUID).getPath();
      } catch (ItemNotFoundException e) {
        LOG.warn("Page does not exist (anymore)", e);
      } catch (RepositoryException e) {
        LOG.warn("Could not get JCR session", e);
      }

      targetUri = pageNodePath == null ? "/" : pageNodePath;
      targetDomain = getProperty(node, RedirectsModule.DOMAIN_PROPERTY);
    }

    redirectType = RedirectType.findByJcrValue(getProperty(node, RedirectsModule.TYPE_PROPERTY));
  }

  String getProperty(Node node, String propertyName) {
    try {
      return node.hasProperty(propertyName) ? node.getProperty(propertyName).getString() : "";
    } catch (RepositoryException e) {
      LOG.warn("Could not retrieve property", e);
    }

    return "";
  }

  public abstract boolean requestMatches(HttpServletRequest request);

  public String getDisplayTarget() {
    if (isAbsoluteUrl) {
      return targetUrl;
    }

    if (StringUtils.isBlank(targetDomain)) {
      return targetUri;
    }

    final boolean forceSecure = Components.getComponent(RedirectsModule.class).isForceSecure();
    return (forceSecure ? "https" : "http") + "://" + targetDomain + targetUri;
  }

  public String getTarget(HttpServletRequest request) {
    if (isAbsoluteUrl) {
      if (request.getQueryString() != null && !targetUrl.contains("?")) {
        return targetUrl + "?" + request.getQueryString();
      }

      return targetUrl;
    }

    if (StringUtils.isBlank(targetDomain)
        || request.getServerName().equalsIgnoreCase(targetDomain)) {
      return targetUri;
    }

    final String port =
        request.getServerPort() != 80 && request.getServerPort() != 443
            ? ":" + request.getServerPort()
            : "";
    final boolean forceSecure = Components.getComponent(RedirectsModule.class).isForceSecure();
    return (request.isSecure() || forceSecure ? "https" : "http")
        + "://"
        + targetDomain
        + port
        + targetUri
        + (request.getQueryString() == null ? "" : "?" + request.getQueryString());
  }

  public RedirectType getRedirectType() {
    return redirectType;
  }

  public Node getNode() {
    return node;
  }
}
