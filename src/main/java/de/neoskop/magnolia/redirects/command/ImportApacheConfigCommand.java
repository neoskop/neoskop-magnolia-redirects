package de.neoskop.magnolia.redirects.command;

import de.neoskop.magnolia.redirects.service.RedirectsService;
import info.magnolia.commands.impl.ImportCommand;
import info.magnolia.context.Context;
import java.io.InputStream;
import javax.inject.Inject;

/**
 * @author Arne Diekmann
 * @since 14.12.16
 */
public class ImportApacheConfigCommand extends ImportCommand {

  private final RedirectsService redirectsService;

  @Inject
  public ImportApacheConfigCommand(RedirectsService redirectsService) {
    this.redirectsService = redirectsService;
  }

  @Override
  public boolean execute(Context context) throws Exception {
    final InputStream stream = getXmlStream();

    try {
      redirectsService.importApacheConfig(stream, getPath());
    } finally {
      if (stream != null) {
        stream.close();
      }
    }

    return true;
  }
}
