package de.neoskop.magnolia.redirects.command;

import com.google.common.base.Charsets;
import de.neoskop.magnolia.redirects.service.RedirectsService;
import info.magnolia.commands.impl.BaseRepositoryCommand;
import info.magnolia.context.Context;
import java.io.IOException;
import java.io.OutputStream;
import javax.inject.Inject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 03.03.17
 */
public class ExportRulesAsCSVCommand extends BaseRepositoryCommand {

  private static final Logger LOG = LogManager.getLogger(ExportRulesAsCSVCommand.class);
  private final RedirectsService redirectsService;
  private OutputStream outputStream;

  @Inject
  public ExportRulesAsCSVCommand(RedirectsService redirectsService) {
    this.redirectsService = redirectsService;
  }

  @Override
  public boolean execute(Context context) throws Exception {
    try (final OutputStream out = getOutputStream()) {
      StringBuilder sb = new StringBuilder();
      final CSVPrinter printer = CSVFormat.DEFAULT.withHeader("FQDN", "Regel", "Ziel").print(sb);

      redirectsService
          .findAllRules()
          .forEach(
              (domain, rules) ->
                  rules.forEach(
                      r -> {
                        try {
                          printer.print(domain);
                          printer.print(r);
                          printer.print(r.getDisplayTarget());
                          printer.println();
                        } catch (IOException e) {
                          LOG.warn("Could not write " + domain + " -> " + r, e);
                        }
                      }));

      out.write(sb.toString().getBytes(Charsets.UTF_8));
      out.flush();
    }

    return true;
  }

  private OutputStream getOutputStream() {
    return outputStream;
  }

  public void setOutputStream(OutputStream outputStream) {
    this.outputStream = outputStream;
  }
}
