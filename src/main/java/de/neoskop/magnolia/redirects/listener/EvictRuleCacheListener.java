package de.neoskop.magnolia.redirects.listener;

import de.neoskop.magnolia.redirects.service.RedirectsService;
import info.magnolia.objectfactory.Components;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;

/**
 * @author Arne Diekmann
 * @since 13.12.16
 */
public class EvictRuleCacheListener implements EventListener {

  @Override
  public void onEvent(EventIterator events) {
    Components.getComponent(RedirectsService.class).evictRuleCache();
  }
}
